from flask import Flask, render_template
import numpy as np
from basic_line import js,div,cdn_jss
from bokeh.embed import server_session
from bokeh.client import pull_session

app = Flask(__name__)

@app.route("/bokehgraph/")
def showBokehGraph():
    session = pull_session(url="http://localhost:5006/bokehserve")
    b_scripts = server_session(session_id=session.id, url="http://localhost:5006/bokehserve")
    return render_template("viz3.html", s=b_scripts)





@app.route("/displaygraph/")
def showGraph():
    return render_template("viz2.html", js=js, div=div, cdn_jss=cdn_jss)

@app.route("/")  #main page -> http://127.0.0.1:5000
def indexPage():
    puppies = ["Fluffy", "Doggy", "Tommy", "Blacky"]
    return render_template("index.html", p=puppies)

@app.route("/user/<name>") #http://127.0.0.1:5000/user/John
def userPage(name):
    return "Your name is <i>" + name + ".</i>"

@app.route("/base/") #http://127.0.0.1:5000/base/
def openBase():
    return render_template("base.html")

@app.route("/child/") #http://127.0.0.1:5000/child/
def openChild():
    return render_template("child.html")

@app.route("/linegraph/") #http://127.0.0.1:5000/linegraph/
def openGraph():
    return render_template("viz.html")

if __name__ == "__main__":
    app.run(debug=True)