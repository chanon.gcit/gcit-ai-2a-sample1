from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.resources import CDN 

f = figure()
f.line([1,2,3,4],[8,6,4,1], color="green")

js, div = components(f)

cdn_jss = CDN.js_files[0]

#take note of these 3 variables js, div, cdn_jss

