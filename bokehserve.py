from bokeh.io import show, curdoc
from bokeh.plotting import figure
from bokeh.layouts import layout

f = figure()
f.line([1,2,3,4],[4,3,2,1], color="blue")

myLayout = layout([[f]])
curdoc().add_root(myLayout)

